toolsDirectory = tools/bin

install/tools/lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b ${toolsDirectory} v1.55.1

tools/lint:
	${toolsDirectory}/golangci-lint run ./pkg/...